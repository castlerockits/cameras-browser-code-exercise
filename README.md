# Cameras front end code exercise

Castle Rock hosts REST APIs providing roadside camera data. 

A sample response to the enpoint `GET /cameras.json` is as follows):


```
[
  {
    "id": 1,
    "public": true,
    "name": "Shoshone County Line",
    "lastUpdated": 1540350351110,
    "location": {
      "fips": 16,
      "latitude": 46.97356,
      "longitude": -116.27587,
      "routeId": "ID 3",
      "linearReference": 51.01,
      "localRoad": false,
      "cityReference": "11 miles north of the Bovill area"
    },
    "views": [
      {
        "name": "North",
        "type": "STILL_IMAGE",
        "url": "http://birice.vaisala.com/photos/03B5969E_06A3B6F4_CAM1.jpg",
        "imageTimestamp": 1540350256000
      },
      {
        "name": "South",
        "type": "STILL_IMAGE",
        "url": "http://birice.vaisala.com/photos/03B5969E_06A3B6F4_CAM2.jpg",
        "imageTimestamp": 1540350290000
      }
    ]
  }
]
```

This response contains a single camera record, typically this array will contain many items. Please note, a single camera site has one or more associated images, contained in the `views` array.

Please write front-end web application which consumes this API and provides a solution to the following user stories:

- When I load the page, I see a list of all cameras described by the `name` field.
- My cameras list should be sorted by the cameras' `lastUpdated` timestamps, then `name` if two timestamps are equal.
- When I click on a camera name, I am shown a detail view.
    - The detail view should display a "primary" image view. When I load the detail view, the first image in the views array should be the "primary" image. Below the "primary" image, thumbnails of all cameras views' images should be displayed. 
    - I should only see image thumbnails on the detail screen with `type === "STILL_IMAGE"`. Only `"STILL_IMAGE"` types should be eligible for presentation as the "primary" image.
    - When I click on any image thumbnail, the image it represents becomes the "primary" image (replace the image in the primary image section with the clicked image).

Bonus: 

- When I am viewing the details of a camera, the URL updates so I can link to that camera's details.
- Do not include the current primary image on the details view in the thumbnails list.

### Data and hosting

This repository includes a full API response in [cameras.json](./cameras.json). It can be used to write and test your solution against. We recommend using a web server to host and test your solution, to eliminate quirks of AJAX against the file system. The NodeJS [http-server](https://www.npmjs.com/package/http-server) package is a good solution.

```
# install NodeJS
npm install http-server --global
cd $PROJECT_ROOT
http-server # assets in $PROJECT_ROOT are now available on http://localhost:8080
```

### Markup

Skeleton HTML markup using the Foundation CSS framework is provided in [index.html](./index.html) and [detail.html](./detail.html). Feel free to use this markup as a basis for your solution, but use is completely optional.

